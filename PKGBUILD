# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=notion-app
pkgver=2.0.18
pkgrel=0
pkgdesc="Notion enhanced, vanilla port of official app"
arch=('amd64')
url="https://notion.so"
license=('MIT')
depends=('libgtk-3-0' 'libdbus-glib-1-2' 'libasound2' 'libatopology2' 'libdbusmenu-gtk4' 'libnss3' 'hicolor-icon-theme')
makedepends=('sed')
options=('!strip')
source=("https://github.com/notion-enhancer/notion-repackaged/releases/download/v$pkgver-1/Notion-$pkgver-1.AppImage")
sha256sums=('07f159f82b3140c4f37bb993656c9ca29d5ac8ce6da0913276d3ff6a83b0959d')
_filename="Notion-$pkgver-1.AppImage"

prepare() {
    rm -rf squashfs-root
    chmod +x $_filename
    ./$_filename --appimage-extract

    # Fix directory permissions (read + execute)
    find squashfs-root -type d -exec chmod a+rx {} \;
}

package() {
    cd "${srcdir}/squashfs-root"
    sed -i -e "s|Exec=.\+|Exec=/usr/bin/${pkgname} %U|" notion-app.desktop
    sed -i -e "s|Icon=.\+|Icon=${pkgname}|" notion-app.desktop

    # Icons
    icons=(512x512 256x256 128x128 64x64 48x48 32x32 16x16)

    for size in "${icons[@]}"; do
	    install -Dm0644 "usr/share/icons/hicolor/${size}/apps/notion-app.png" -t "${pkgdir}/usr/share/icons/hicolor/${size}/apps/"
    done


    cd "${srcdir}"
    mkdir -p "$pkgdir"/opt
    mv squashfs-root "$pkgdir"/opt/notion-app

    # Extract package data
    install -dm755 "${pkgdir}/usr/bin"
    install -dm644 "${pkgdir}/usr/share/applications"
    ln -sf "/opt/notion-app/notion-app" "${pkgdir}/usr/bin/notion-app"
    ln -sf "/opt/notion-app/notion-app.desktop" "${pkgdir}/usr/share/applications/notion-app.desktop"
    
    install -D -m644 "${pkgdir}/opt/notion-app/LICENSES.chromium.html" "${pkgdir}/usr/share/licenses/${pkgname}/copyright"
}
